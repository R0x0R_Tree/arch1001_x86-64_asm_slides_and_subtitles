1
00:00:00,240 --> 00:00:04,240
so like i said because there's a bitwise

2
00:00:02,639 --> 00:00:05,359
and but it's not actually storing the

3
00:00:04,240 --> 00:00:07,440
result anywhere

4
00:00:05,359 --> 00:00:09,679
the compiler generates a test which

5
00:00:07,440 --> 00:00:11,200
doesn't end but doesn't store the result

6
00:00:09,679 --> 00:00:12,960
and then i said you should understand

7
00:00:11,200 --> 00:00:14,799
all the other assembly in here

8
00:00:12,960 --> 00:00:16,400
but if you went through this example and

9
00:00:14,799 --> 00:00:17,279
you read it carefully you should

10
00:00:16,400 --> 00:00:20,320
basically say

11
00:00:17,279 --> 00:00:21,920
no professor x you know i don't

12
00:00:20,320 --> 00:00:24,160
understand why there's two

13
00:00:21,920 --> 00:00:25,199
jumps like this jump should never ever

14
00:00:24,160 --> 00:00:27,039
be reachable

15
00:00:25,199 --> 00:00:28,560
it's not referenced anywhere nothing

16
00:00:27,039 --> 00:00:31,279
else says like jump to

17
00:00:28,560 --> 00:00:32,559
1029 and this jump immediately before it

18
00:00:31,279 --> 00:00:34,480
is going to jump over it

19
00:00:32,559 --> 00:00:35,680
and so you're right it doesn't make a

20
00:00:34,480 --> 00:00:37,920
lick of sense

21
00:00:35,680 --> 00:00:39,840
and that's because we are again doing

22
00:00:37,920 --> 00:00:41,760
unoptimized code generation

23
00:00:39,840 --> 00:00:43,200
so if you tell the compiler to be stupid

24
00:00:41,760 --> 00:00:44,960
it's going to be stupid and it's going

25
00:00:43,200 --> 00:00:46,879
to just generate things on

26
00:00:44,960 --> 00:00:48,160
different patterns that it has about how

27
00:00:46,879 --> 00:00:48,800
it should behave based on the source

28
00:00:48,160 --> 00:00:51,440
code

29
00:00:48,800 --> 00:00:52,640
so this is straight up an artifact and

30
00:00:51,440 --> 00:00:59,840
unreachable

31
00:00:52,640 --> 00:00:59,840
and an unreachable instruction

32
00:01:04,640 --> 00:01:07,680
so we picked up one more assembly

33
00:01:06,479 --> 00:01:10,560
instruction

34
00:01:07,680 --> 00:01:13,600
that is the test instruction coming in

35
00:01:10,560 --> 00:01:16,479
at a whopping six percent

36
00:01:13,600 --> 00:01:18,799
and with that achievement unlocked we've

37
00:01:16,479 --> 00:01:20,720
now completed the 100

38
00:01:18,799 --> 00:01:22,240
of all these assembly instructions and

39
00:01:20,720 --> 00:01:23,920
what are we on we're only on assembly

40
00:01:22,240 --> 00:01:27,040
instruction 22.

41
00:01:23,920 --> 00:01:30,000
that is super cool now theoretically

42
00:01:27,040 --> 00:01:32,000
you can go read web browser assembly and

43
00:01:30,000 --> 00:01:33,759
understand everything that goes on there

44
00:01:32,000 --> 00:01:35,520
except for this other which covers a

45
00:01:33,759 --> 00:01:36,960
bunch of miscellaneous stuff who knows

46
00:01:35,520 --> 00:01:39,280
what that covers

47
00:01:36,960 --> 00:01:40,000
but great kid don't get cocky you

48
00:01:39,280 --> 00:01:42,320
basically just

49
00:01:40,000 --> 00:01:43,520
learned how to pronounce the alphabet so

50
00:01:42,320 --> 00:01:45,119
yes you've just

51
00:01:43,520 --> 00:01:47,040
learned you know 22 assembly

52
00:01:45,119 --> 00:01:47,759
instructions and yes that covered all

53
00:01:47,040 --> 00:01:49,680
the

54
00:01:47,759 --> 00:01:51,200
assembly instructions in that particular

55
00:01:49,680 --> 00:01:53,759
case but the reality

56
00:01:51,200 --> 00:01:55,360
is that reading assembly is kind of like

57
00:01:53,759 --> 00:01:57,520
learning another language

58
00:01:55,360 --> 00:01:59,280
and so you may go off and learn how to

59
00:01:57,520 --> 00:02:00,240
pronounce the alphabet you may learn a

60
00:01:59,280 --> 00:02:01,680
few couple words

61
00:02:00,240 --> 00:02:03,119
but you're not really going to

62
00:02:01,680 --> 00:02:04,799
understand the language until you

63
00:02:03,119 --> 00:02:05,840
practice it a bunch you have to read a

64
00:02:04,799 --> 00:02:07,040
ton of assembly

65
00:02:05,840 --> 00:02:08,720
you need to write a little bit of

66
00:02:07,040 --> 00:02:10,160
assembly but you have to do it over and

67
00:02:08,720 --> 00:02:12,080
over and over and over again

68
00:02:10,160 --> 00:02:13,360
just like learning any other normal

69
00:02:12,080 --> 00:02:15,599
human language

70
00:02:13,360 --> 00:02:16,879
so while this is a super important and

71
00:02:15,599 --> 00:02:18,959
noteworthy achievement

72
00:02:16,879 --> 00:02:20,879
you are probably not going to be able to

73
00:02:18,959 --> 00:02:23,760
just go off and disassemble

74
00:02:20,879 --> 00:02:24,959
a complete web browser and expect to be

75
00:02:23,760 --> 00:02:26,400
able to read things you're going to need

76
00:02:24,959 --> 00:02:29,120
a lot of practice between

77
00:02:26,400 --> 00:02:30,879
now and then and hopefully this class

78
00:02:29,120 --> 00:02:32,959
will give you plenty of practice

79
00:02:30,879 --> 00:02:34,959
and that's why we're going to have a lab

80
00:02:32,959 --> 00:02:36,800
example later on just to show you how

81
00:02:34,959 --> 00:02:38,720
difficult it is to go from

82
00:02:36,800 --> 00:02:41,760
memorizing assembly instructions to

83
00:02:38,720 --> 00:02:41,760
actually reading code

