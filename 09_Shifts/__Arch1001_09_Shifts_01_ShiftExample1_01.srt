1
00:00:00,160 --> 00:00:07,520
oh get shifty

2
00:00:04,080 --> 00:00:08,320
it's shifty time today [ __ ] shift that

3
00:00:07,520 --> 00:00:11,679
is

4
00:00:08,320 --> 00:00:15,040
here are our bit shift operations in c

5
00:00:11,679 --> 00:00:17,840
left shifting right shifting and these

6
00:00:15,040 --> 00:00:18,720
turn into new assembly instructions

7
00:00:17,840 --> 00:00:21,600
shift left

8
00:00:18,720 --> 00:00:23,279
and shift right so shift logical left

9
00:00:21,600 --> 00:00:24,160
we'll see a different form shift

10
00:00:23,279 --> 00:00:27,680
arithmetic

11
00:00:24,160 --> 00:00:30,320
left later on can be used with the c

12
00:00:27,680 --> 00:00:32,079
less than less than sign operator shift

13
00:00:30,320 --> 00:00:33,680
left takes two operands

14
00:00:32,079 --> 00:00:36,480
the first can be the source and

15
00:00:33,680 --> 00:00:39,520
destination which is specified in an rmx

16
00:00:36,480 --> 00:00:42,640
and the second can either be cl the

17
00:00:39,520 --> 00:00:44,160
least significant byte of rcx or a

18
00:00:42,640 --> 00:00:46,239
single byte immediate

19
00:00:44,160 --> 00:00:48,079
because the registers are only 64 bits

20
00:00:46,239 --> 00:00:51,120
long it doesn't make sense to shift

21
00:00:48,079 --> 00:00:52,879
more than 64 so you can only have one

22
00:00:51,120 --> 00:00:54,719
byte not four bytes for instance

23
00:00:52,879 --> 00:00:56,879
now if you even know about this c

24
00:00:54,719 --> 00:01:00,079
operator you may know that it

25
00:00:56,879 --> 00:01:00,640
multiplies a value by two for each place

26
00:01:00,079 --> 00:01:02,960
that it's

27
00:01:00,640 --> 00:01:04,879
shifted to the left so if you think

28
00:01:02,960 --> 00:01:06,479
about literally the number one and if

29
00:01:04,879 --> 00:01:08,080
you shift it one bit over

30
00:01:06,479 --> 00:01:09,760
then you're going to get the number two

31
00:01:08,080 --> 00:01:11,360
if you shifted another one bit you're

32
00:01:09,760 --> 00:01:14,320
going to get the number four

33
00:01:11,360 --> 00:01:16,080
compilers will frequently use shifts

34
00:01:14,320 --> 00:01:18,479
instead of multiplies if you're

35
00:01:16,080 --> 00:01:20,159
multiplying by a power of two

36
00:01:18,479 --> 00:01:22,880
now bringing back up one of those bits i

37
00:01:20,159 --> 00:01:25,680
said we mostly don't care about

38
00:01:22,880 --> 00:01:26,000
if you shift stuff off the left hand

39
00:01:25,680 --> 00:01:28,640
side

40
00:01:26,000 --> 00:01:29,600
of the register the sort of last bit

41
00:01:28,640 --> 00:01:32,400
that you shift

42
00:01:29,600 --> 00:01:34,640
will go into the carry flag in our r

43
00:01:32,400 --> 00:01:36,079
flags status register so let's look at

44
00:01:34,640 --> 00:01:39,119
this example right here

45
00:01:36,079 --> 00:01:41,040
if we have a value 0 0 1 1 0 0 1

46
00:01:39,119 --> 00:01:42,240
one and we shift it two bits to the left

47
00:01:41,040 --> 00:01:45,439
we end up with

48
00:01:42,240 --> 00:01:47,600
one one zero zero one one zero zero but

49
00:01:45,439 --> 00:01:49,280
this bit this last bit that was shifted

50
00:01:47,600 --> 00:01:50,159
off this was the first bit this was the

51
00:01:49,280 --> 00:01:53,840
second bit

52
00:01:50,159 --> 00:01:56,560
this zero goes into the carry flag

53
00:01:53,840 --> 00:01:57,280
if we instead shift three bits then we

54
00:01:56,560 --> 00:02:00,399
get

55
00:01:57,280 --> 00:02:02,640
one zero zero one one zero zero zero

56
00:02:00,399 --> 00:02:04,719
and this was the third bit so it's like

57
00:02:02,640 --> 00:02:07,040
shift one shift two shift three

58
00:02:04,719 --> 00:02:08,399
and that third bit the one that goes

59
00:02:07,040 --> 00:02:10,479
into the carry flag

60
00:02:08,399 --> 00:02:12,160
then there's shift logical right which

61
00:02:10,479 --> 00:02:12,959
is the greater than greater than

62
00:02:12,160 --> 00:02:15,360
operator

63
00:02:12,959 --> 00:02:17,760
it has the same general form as the

64
00:02:15,360 --> 00:02:20,560
shift left it has two operands the first

65
00:02:17,760 --> 00:02:21,760
can be is the source and destination and

66
00:02:20,560 --> 00:02:24,879
it can be specified in an

67
00:02:21,760 --> 00:02:25,840
rmx form the second is cl the least

68
00:02:24,879 --> 00:02:28,640
significant byte

69
00:02:25,840 --> 00:02:31,280
of rcx or a one byte immediate and if

70
00:02:28,640 --> 00:02:33,280
shifting to the left multiplies by two

71
00:02:31,280 --> 00:02:35,519
then shifting to the right divides by

72
00:02:33,280 --> 00:02:37,920
two and the same sort of rule

73
00:02:35,519 --> 00:02:39,680
occurs here where whatever the last bit

74
00:02:37,920 --> 00:02:42,560
was that was shifted to the right

75
00:02:39,680 --> 00:02:42,959
it's going to go into the carry flag so

76
00:02:42,560 --> 00:02:45,040
here

77
00:02:42,959 --> 00:02:46,720
zero zero one one zero zero one one if

78
00:02:45,040 --> 00:02:49,760
we shift two bits

79
00:02:46,720 --> 00:02:50,160
shift one shift two then this one right

80
00:02:49,760 --> 00:02:52,480
here

81
00:02:50,160 --> 00:02:53,680
goes into the carry flag and the rest of

82
00:02:52,480 --> 00:02:55,200
them are shifted down

83
00:02:53,680 --> 00:02:57,040
i should have mentioned back here on the

84
00:02:55,200 --> 00:02:57,599
shift logical left when you're going to

85
00:02:57,040 --> 00:02:59,920
the left

86
00:02:57,599 --> 00:03:01,200
the least significant bits are filled in

87
00:02:59,920 --> 00:03:03,040
with zeros

88
00:03:01,200 --> 00:03:04,560
and on the shift logical right when

89
00:03:03,040 --> 00:03:05,760
you're shifting to the right the most

90
00:03:04,560 --> 00:03:08,400
significant bits are

91
00:03:05,760 --> 00:03:09,200
filled with zeros so go ahead and take

92
00:03:08,400 --> 00:03:11,519
your time

93
00:03:09,200 --> 00:03:17,040
and wander on through this assembly to

94
00:03:11,519 --> 00:03:17,040
check your understanding

