1
00:00:00,080 --> 00:00:03,439
so i said reps toss is basically a mem

2
00:00:02,480 --> 00:00:05,920
set in a box

3
00:00:03,439 --> 00:00:07,120
and so that memset is being used in

4
00:00:05,920 --> 00:00:10,080
furtherance of the

5
00:00:07,120 --> 00:00:11,040
zero initialization setting all of the b

6
00:00:10,080 --> 00:00:14,960
of 64

7
00:00:11,040 --> 00:00:17,600
to zero so as i gave you a hint

8
00:00:14,960 --> 00:00:18,240
push edi at the beginning pop art sorry

9
00:00:17,600 --> 00:00:21,199
push

10
00:00:18,240 --> 00:00:22,880
rdi at the beginning pop rdi at the end

11
00:00:21,199 --> 00:00:25,680
that doesn't smell like seafood that

12
00:00:22,880 --> 00:00:27,840
smells like kali save register to me

13
00:00:25,680 --> 00:00:29,599
allocate some space on the stack

14
00:00:27,840 --> 00:00:32,719
calculate an address put it into

15
00:00:29,599 --> 00:00:34,800
rax move rdx to d

16
00:00:32,719 --> 00:00:35,920
rdi well that is probably the

17
00:00:34,800 --> 00:00:37,760
destination

18
00:00:35,920 --> 00:00:39,120
for the reps toss that's probably the

19
00:00:37,760 --> 00:00:43,040
beginning of the

20
00:00:39,120 --> 00:00:45,840
b b of zero for instance then xor eax

21
00:00:43,040 --> 00:00:48,000
eax well that's going to zero out the

22
00:00:45,840 --> 00:00:50,160
rax register

23
00:00:48,000 --> 00:00:52,559
which means that the value which is

24
00:00:50,160 --> 00:00:54,079
going to be stored each time in the reps

25
00:00:52,559 --> 00:00:55,760
is going to be zero

26
00:00:54,079 --> 00:00:57,360
and it's going to store one byte at a

27
00:00:55,760 --> 00:00:58,399
time because you can see it's a byte

28
00:00:57,360 --> 00:01:01,520
pointer

29
00:00:58,399 --> 00:01:04,320
so it's copying one byte of zero

30
00:01:01,520 --> 00:01:05,199
to rdi and how many times is it doing it

31
00:01:04,320 --> 00:01:08,640
it's copying it

32
00:01:05,199 --> 00:01:12,159
hex 80 times because hex 80

33
00:01:08,640 --> 00:01:15,520
is 2 times 64 is 128

34
00:01:12,159 --> 00:01:18,799
128 in hex is hex 80. so there you go

35
00:01:15,520 --> 00:01:21,920
these setup instructions and these nut

36
00:01:18,799 --> 00:01:24,159
and this knock down instruction the reps

37
00:01:21,920 --> 00:01:26,000
are going to zero initialize the entire

38
00:01:24,159 --> 00:01:28,799
b of 64.

39
00:01:26,000 --> 00:01:30,159
short array and after that it's just the

40
00:01:28,799 --> 00:01:31,920
kind of stuff that we have seen

41
00:01:30,159 --> 00:01:32,560
previously in array local variable

42
00:01:31,920 --> 00:01:35,600
there's

43
00:01:32,560 --> 00:01:38,799
indexing into b of 1 here b

44
00:01:35,600 --> 00:01:39,680
is a array of shorts so move 2 the size

45
00:01:38,799 --> 00:01:42,720
of a short

46
00:01:39,680 --> 00:01:44,799
multiply by 1 the index of the array

47
00:01:42,720 --> 00:01:46,479
and you know do the access assignment

48
00:01:44,799 --> 00:01:48,560
and eventual return value

49
00:01:46,479 --> 00:01:50,240
well another one bites the dust we have

50
00:01:48,560 --> 00:01:53,520
added a new assembly instruction

51
00:01:50,240 --> 00:01:58,399
reps dos to our assembly bag of tricks

52
00:01:53,520 --> 00:01:58,399
number 29 as other

