1
00:00:00,160 --> 00:00:03,919
all right the next example is array

2
00:00:02,240 --> 00:00:08,480
local variable

3
00:00:03,919 --> 00:00:10,880
so let's go into there let's cat it out

4
00:00:08,480 --> 00:00:11,679
remember what it was all right so this

5
00:00:10,880 --> 00:00:15,040
was the one with

6
00:00:11,679 --> 00:00:18,160
a as a short six inch in b and

7
00:00:15,040 --> 00:00:21,039
c is a long long x babe goes into a

8
00:00:18,160 --> 00:00:21,760
balboa blood blood goes into c and then

9
00:00:21,039 --> 00:00:25,199
a into b

10
00:00:21,760 --> 00:00:27,439
of one b of one plus c into b of four

11
00:00:25,199 --> 00:00:31,920
return b of four all right so let's go

12
00:00:27,439 --> 00:00:33,360
ahead and compile that and run jdb

13
00:00:31,920 --> 00:00:35,280
so taking a look at this we have our

14
00:00:33,360 --> 00:00:36,640
typical cet

15
00:00:35,280 --> 00:00:38,480
instruction at the beginning we can

16
00:00:36,640 --> 00:00:40,000
ignore we've got a typical function

17
00:00:38,480 --> 00:00:43,520
prologue setting up the

18
00:00:40,000 --> 00:00:45,520
stack frame we've got hex babe into rbp

19
00:00:43,520 --> 00:00:47,600
minus two and because we're going to

20
00:00:45,520 --> 00:00:49,280
have these accesses at rbp

21
00:00:47,600 --> 00:00:51,039
minus something and we don't actually

22
00:00:49,280 --> 00:00:52,000
see any subtraction from the stack

23
00:00:51,039 --> 00:00:54,399
pointer here so

24
00:00:52,000 --> 00:00:55,600
this is a another leaf function that's

25
00:00:54,399 --> 00:00:58,960
using the red zone

26
00:00:55,600 --> 00:01:02,000
as its stack area i want to get a sense

27
00:00:58,960 --> 00:01:03,520
of you know how much below rbp i should

28
00:01:02,000 --> 00:01:04,159
be looking at to see all these local

29
00:01:03,520 --> 00:01:06,799
variables

30
00:01:04,159 --> 00:01:06,799
so to do that

31
00:01:07,680 --> 00:01:11,520
i can do disassemble main and then just

32
00:01:10,000 --> 00:01:14,960
kind of eyeball through so

33
00:01:11,520 --> 00:01:18,240
minus x2 minus hex 10 2

34
00:01:14,960 --> 00:01:20,400
2 c 10 and 2 0.

35
00:01:18,240 --> 00:01:22,159
so minus 2 c is going to be the lowest

36
00:01:20,400 --> 00:01:22,640
access that we're going to see in this

37
00:01:22,159 --> 00:01:26,080
function

38
00:01:22,640 --> 00:01:28,320
so i'm going to add a new display and i

39
00:01:26,080 --> 00:01:29,280
need to set a format specifier so that i

40
00:01:28,320 --> 00:01:31,759
can display

41
00:01:29,280 --> 00:01:33,600
i'm going to say i want to display uh

42
00:01:31,759 --> 00:01:36,000
ins at a time because b

43
00:01:33,600 --> 00:01:36,640
is an array of six ins and we're writing

44
00:01:36,000 --> 00:01:39,360
stuff into b

45
00:01:36,640 --> 00:01:40,560
of one and two b of four so how many ins

46
00:01:39,360 --> 00:01:43,759
do i need to cover

47
00:01:40,560 --> 00:01:47,119
hex 2c well hex 2

48
00:01:43,759 --> 00:01:50,799
would be 8 because it's 4

49
00:01:47,119 --> 00:01:53,119
ins per hex 10 so answer 4 bytes x 10 is

50
00:01:50,799 --> 00:01:56,960
16 bytes so 4 per 10

51
00:01:53,119 --> 00:02:00,000
times 2 is 8 and then c that is 12

52
00:01:56,960 --> 00:02:00,399
so plus 3 8 plus 3 is 11. so i'm going

53
00:02:00,000 --> 00:02:04,320
to need

54
00:02:00,399 --> 00:02:05,680
11 words because gdb ints are called

55
00:02:04,320 --> 00:02:08,879
words even though

56
00:02:05,680 --> 00:02:10,640
in intel assembly words are two bytes

57
00:02:08,879 --> 00:02:13,920
gdb words are four bytes

58
00:02:10,640 --> 00:02:17,040
so eleven words in hex format

59
00:02:13,920 --> 00:02:20,000
at rbp minus

60
00:02:17,040 --> 00:02:20,000
x2c

61
00:02:21,280 --> 00:02:25,200
and what did i do there wrong

62
00:02:26,400 --> 00:02:30,239
ah because i haven't actually stepped

63
00:02:28,080 --> 00:02:30,879
into the function yet so rbp hasn't

64
00:02:30,239 --> 00:02:33,360
changed

65
00:02:30,879 --> 00:02:35,040
to anything valid so let's go ahead and

66
00:02:33,360 --> 00:02:36,800
step into it

67
00:02:35,040 --> 00:02:38,560
up through the function prologue and

68
00:02:36,800 --> 00:02:40,720
there we go now i've got an extra one so

69
00:02:38,560 --> 00:02:43,760
let's un-display it

70
00:02:40,720 --> 00:02:47,200
on display 14.

71
00:02:43,760 --> 00:02:50,560
all right so hex babe into rbp minus

72
00:02:47,200 --> 00:02:54,800
x2 and so moving a word

73
00:02:50,560 --> 00:02:58,800
which intel words 16 bits so rbp minus 2

74
00:02:54,800 --> 00:03:05,840
step and there we go hex babe

75
00:02:58,800 --> 00:03:05,840
rbp minus 2.

76
00:03:10,319 --> 00:03:13,599
to confirm that we could for instance do

77
00:03:13,200 --> 00:03:16,640
x

78
00:03:13,599 --> 00:03:20,000
slash one

79
00:03:16,640 --> 00:03:24,239
half word in hex

80
00:03:20,000 --> 00:03:26,959
at rbp minus

81
00:03:24,239 --> 00:03:28,879
x2 and you'll see it's just babe there

82
00:03:26,959 --> 00:03:29,599
we've got the next instruction is move

83
00:03:28,879 --> 00:03:30,879
abs

84
00:03:29,599 --> 00:03:32,560
and you might be thinking that's you

85
00:03:30,879 --> 00:03:33,440
know move absolute value or something

86
00:03:32,560 --> 00:03:35,440
like that

87
00:03:33,440 --> 00:03:37,519
but this is just the moving of the

88
00:03:35,440 --> 00:03:40,720
constant balboa blood blood

89
00:03:37,519 --> 00:03:41,280
into the rax register so what is a move

90
00:03:40,720 --> 00:03:43,920
abs

91
00:03:41,280 --> 00:03:45,599
instruction well the answer is there's

92
00:03:43,920 --> 00:03:49,040
no such thing

93
00:03:45,599 --> 00:03:50,640
it turns out that move abs so basically

94
00:03:49,040 --> 00:03:54,000
this is documentation

95
00:03:50,640 --> 00:03:55,680
for as which is the canoe assembler

96
00:03:54,000 --> 00:03:57,920
and it's basically explaining that in

97
00:03:55,680 --> 00:03:58,720
64-bit code move abs can be used to

98
00:03:57,920 --> 00:04:00,799
include the

99
00:03:58,720 --> 00:04:03,519
encode the move instruction with a

100
00:04:00,799 --> 00:04:05,680
64-bit displacement or immediate operand

101
00:04:03,519 --> 00:04:07,599
it's obviously using a 64-bit immediate

102
00:04:05,680 --> 00:04:11,200
operand here so they've chosen to

103
00:04:07,599 --> 00:04:12,879
use move abs instead so all this is

104
00:04:11,200 --> 00:04:13,280
going to do is just move balboa blood

105
00:04:12,879 --> 00:04:16,799
blood

106
00:04:13,280 --> 00:04:19,919
into rax so step over that

107
00:04:16,799 --> 00:04:21,359
and then rax goes right into rbp minus

108
00:04:19,919 --> 00:04:23,759
x10 well we know

109
00:04:21,359 --> 00:04:25,600
ebola blood blood goes into c so that

110
00:04:23,759 --> 00:04:29,440
must be our c local variable

111
00:04:25,600 --> 00:04:32,320
step over that and now we have

112
00:04:29,440 --> 00:04:33,120
move with sign extension so this is the

113
00:04:32,320 --> 00:04:35,199
move s

114
00:04:33,120 --> 00:04:37,040
move with sign extension but then

115
00:04:35,199 --> 00:04:39,120
because they're you know they don't have

116
00:04:37,040 --> 00:04:40,160
the x they're you know trying to show

117
00:04:39,120 --> 00:04:42,880
the

118
00:04:40,160 --> 00:04:44,160
sizes of the operands that are being

119
00:04:42,880 --> 00:04:46,080
dealt with here so

120
00:04:44,160 --> 00:04:48,479
it's moving with sign extension from a

121
00:04:46,080 --> 00:04:51,520
word so that's the 16-bit value

122
00:04:48,479 --> 00:04:53,680
to a long that's the 32-bit value so

123
00:04:51,520 --> 00:04:55,360
it's grabbing from rbp minus 2 well we

124
00:04:53,680 --> 00:04:56,400
know that's hex babe and that's just the

125
00:04:55,360 --> 00:04:59,040
16 bit value

126
00:04:56,400 --> 00:05:00,160
so moving with sign extension hex babe

127
00:04:59,040 --> 00:05:02,479
into eax

128
00:05:00,160 --> 00:05:04,000
this was the thing we saw before in the

129
00:05:02,479 --> 00:05:06,000
visual studio that

130
00:05:04,000 --> 00:05:07,680
because the compiler sees that we're

131
00:05:06,000 --> 00:05:08,800
operating on signed values and because

132
00:05:07,680 --> 00:05:11,840
we're going to be putting that

133
00:05:08,800 --> 00:05:13,919
a into b of one which is assigned int

134
00:05:11,840 --> 00:05:16,000
it's doing a sine extension before the

135
00:05:13,919 --> 00:05:18,240
math with adding it to c

136
00:05:16,000 --> 00:05:19,520
so we've got the sine extended value and

137
00:05:18,240 --> 00:05:23,039
then it's going to move that

138
00:05:19,520 --> 00:05:24,000
into rbp minus 2c and it's going to

139
00:05:23,039 --> 00:05:27,199
pluck that right back

140
00:05:24,000 --> 00:05:29,039
out so that was the moving a into b of

141
00:05:27,199 --> 00:05:32,320
one so we can infer that

142
00:05:29,039 --> 00:05:36,320
rbp minus two c is the b of one

143
00:05:32,320 --> 00:05:39,120
so we step and there we go goes into two

144
00:05:36,320 --> 00:05:40,400
fff babe goes into that and then pulls

145
00:05:39,120 --> 00:05:42,080
it right back out because it's going to

146
00:05:40,400 --> 00:05:44,720
do some math on it

147
00:05:42,080 --> 00:05:45,280
on optimized code of course and then it

148
00:05:44,720 --> 00:05:49,039
takes

149
00:05:45,280 --> 00:05:49,360
and it moves eax to edx because it wants

150
00:05:49,039 --> 00:05:52,880
to

151
00:05:49,360 --> 00:05:56,160
accumulate in the a register

152
00:05:52,880 --> 00:05:58,160
so it's got that in rdx over here it's

153
00:05:56,160 --> 00:06:02,000
going to take rbp minus x10

154
00:05:58,160 --> 00:06:06,800
into rax that is balboa blood blood

155
00:06:02,000 --> 00:06:10,000
and then add edx to eax

156
00:06:06,800 --> 00:06:12,639
step and that's going to give us our

157
00:06:10,000 --> 00:06:13,919
math result there this is the value

158
00:06:12,639 --> 00:06:16,240
that's going to be put into

159
00:06:13,919 --> 00:06:17,199
b of 4 and then it's going to be used as

160
00:06:16,240 --> 00:06:19,039
a return address

161
00:06:17,199 --> 00:06:21,199
so first it's going to move it into

162
00:06:19,039 --> 00:06:22,000
memory that's the moving it into b of

163
00:06:21,199 --> 00:06:24,560
four

164
00:06:22,000 --> 00:06:25,600
there we go right there so b of one b of

165
00:06:24,560 --> 00:06:28,639
two b of three

166
00:06:25,600 --> 00:06:29,360
b of four and then it just plucks it

167
00:06:28,639 --> 00:06:32,400
right back out

168
00:06:29,360 --> 00:06:35,039
and puts it into eax the return register

169
00:06:32,400 --> 00:06:35,840
of course it's gonna do a sign extension

170
00:06:35,039 --> 00:06:38,160
of that

171
00:06:35,840 --> 00:06:38,880
but it's zero in the most significant

172
00:06:38,160 --> 00:06:41,039
bit so

173
00:06:38,880 --> 00:06:41,919
you just get that value and now tear

174
00:06:41,039 --> 00:06:44,800
down function

175
00:06:41,919 --> 00:06:45,840
epilogue tear down the stack frame and

176
00:06:44,800 --> 00:06:48,479
we're done

177
00:06:45,840 --> 00:06:49,280
boom boom back in the function that

178
00:06:48,479 --> 00:06:51,520
called main

179
00:06:49,280 --> 00:06:52,720
so that's it for this example nothing

180
00:06:51,520 --> 00:06:55,680
especially new

181
00:06:52,720 --> 00:06:56,000
really or interesting going on here just

182
00:06:55,680 --> 00:06:58,000
a

183
00:06:56,000 --> 00:06:59,120
introduction to the fact that sometimes

184
00:06:58,000 --> 00:07:00,880
you're going to get

185
00:06:59,120 --> 00:07:02,880
assembly instructions and if you happen

186
00:07:00,880 --> 00:07:04,560
to go look for them in the manual you

187
00:07:02,880 --> 00:07:06,639
might not actually be able to find them

188
00:07:04,560 --> 00:07:07,840
and therefore you need to you know

189
00:07:06,639 --> 00:07:10,240
search around for whether

190
00:07:07,840 --> 00:07:13,360
a new assembler is doing something weird

191
00:07:10,240 --> 00:07:13,360
for instance

