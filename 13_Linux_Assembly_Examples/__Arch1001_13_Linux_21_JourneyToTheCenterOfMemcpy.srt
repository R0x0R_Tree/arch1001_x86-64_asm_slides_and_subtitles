1
00:00:00,34 --> 00:00:02,61
So let's take a journey to the center of

2
00:00:02,61 --> 00:00:04,2
memcpy on Linux,

3
00:00:04,2 --> 00:00:04,75
shall we?

4
00:00:05,74 --> 00:00:09,98
So we saw before that this function is basically going

5
00:00:09,98 --> 00:00:14,29
to do a memcpy of some struct's 'a' to

6
00:00:14,29 --> 00:00:16,73
'b' where the structs are mystruct_ts,

7
00:00:16,73 --> 00:00:19,86
which is defined as a single int with a four

8
00:00:19,86 --> 00:00:22,16
byte character array. In visual studio

9
00:00:22,16 --> 00:00:24,9
We saw that we had to bump up the size

10
00:00:24,9 --> 00:00:27,05
of this thing in order to ultimately lead to a

11
00:00:27,06 --> 00:00:29,78
rep movs and we'll see whether or not that's

12
00:00:29,78 --> 00:00:30,85
true here as well

13
00:00:30,86 --> 00:00:33,27
You can imagine that on some operating systems that just

14
00:00:33,27 --> 00:00:36,51
always unconditionally goes to a memcpy function call and

15
00:00:36,52 --> 00:00:37,04
others

16
00:00:37,04 --> 00:00:37,71
It's going to,

17
00:00:37,72 --> 00:00:39,62
even with optimizations off,

18
00:00:39,63 --> 00:00:41,95
try to just inline something

19
00:00:41,97 --> 00:00:42,46
Well,

20
00:00:42,47 --> 00:00:44,16
that was a little bit of a hint

21
00:00:44,74 --> 00:00:46,66
So what's going to happen here?

22
00:00:47,34 --> 00:00:50,95
Let's compile it and debug it to find out,

23
00:00:50,97 --> 00:00:51,46
Oh no,

24
00:00:51,46 --> 00:00:53,58
it looks like this has been inlined

25
00:00:53,59 --> 00:00:53,96
Right?

26
00:00:53,96 --> 00:00:57,04
So there is no call to memcpy whatsoever

27
00:00:57,07 --> 00:00:59,12
There's just a couple of move instructions

28
00:00:59,13 --> 00:01:01,49
This is slightly different from what we saw in visual

29
00:01:01,49 --> 00:01:02,59
studio there

30
00:01:02,59 --> 00:01:05,04
It did always go to a memcpy function and

31
00:01:05,04 --> 00:01:07,84
then if the value was a small size,

32
00:01:07,84 --> 00:01:09,99
it would just translate it into a couple of move

33
00:01:09,99 --> 00:01:10,8
instructions

34
00:01:10,81 --> 00:01:11,63
And so here,

35
00:01:11,64 --> 00:01:14,39
it didn't even bother to jump off to some other

36
00:01:14,39 --> 00:01:14,96
function

37
00:01:14,97 --> 00:01:16,86
It just went ahead and inlined to that and

38
00:01:16,86 --> 00:01:18,03
then returned 0xace0fba5e

39
00:01:18,04 --> 00:01:20,76
So we're going to need to bump up this size

40
00:01:20,77 --> 00:01:24,59
if we want to Get something that is going to

41
00:01:24,59 --> 00:01:25,95
go to the memcpy function

42
00:01:25,96 --> 00:01:31,13
So let's go ahead and try hex 20 was the

43
00:01:31,13 --> 00:01:31,66
size

44
00:01:31,66 --> 00:01:34,44
That was one of the conditional checks if I recall

45
00:01:34,44 --> 00:01:35,86
correctly in visual studio

46
00:01:36,44 --> 00:01:39,52
So let's go ahead and try that and see what

47
00:01:39,52 --> 00:01:40,06
happens

48
00:01:40,94 --> 00:01:41,66
Well,

49
00:01:41,66 --> 00:01:44,21
it looks like it's just text 20 worth of move

50
00:01:44,21 --> 00:01:45,03
instructions

51
00:01:45,04 --> 00:01:51,58
So let's go ahead and go nuts on that And

52
00:01:51,58 --> 00:01:54,85
make it much bigger like X 200

53
00:02:02,54 --> 00:02:03,41
All right

54
00:02:03,42 --> 00:02:05,43
Step the step we step in

55
00:02:05,62 --> 00:02:06,19
Oh,

56
00:02:06,19 --> 00:02:07,01
and there we go

57
00:02:07,01 --> 00:02:08,54
We have a rep move ass

58
00:02:08,54 --> 00:02:08,8
Well,

59
00:02:08,8 --> 00:02:09,56
that was easy

60
00:02:09,94 --> 00:02:12,17
I could have swore it was harder to find before

61
00:02:12,17 --> 00:02:15,94
but I was probably using some other optimization options by

62
00:02:15,94 --> 00:02:16,66
accident

63
00:02:16,67 --> 00:02:19,54
So let's go ahead and restarted and see what's going

64
00:02:19,54 --> 00:02:20,05
on here

65
00:02:20,94 --> 00:02:21,3
Okay

66
00:02:21,3 --> 00:02:24,06
We got a typical function prologue

67
00:02:24,07 --> 00:02:27,49
We've got a subtract of a giant chunk of stack

68
00:02:27,49 --> 00:02:27,86
space

69
00:02:27,86 --> 00:02:31,62
That's because our garb that are HX 200 big are

70
00:02:31,62 --> 00:02:33,43
on the stack

71
00:02:33,45 --> 00:02:38,74
We've got a move of FF into our VP minus

72
00:02:38,74 --> 00:02:39,69
hex to 10

73
00:02:39,69 --> 00:02:39,94
Well,

74
00:02:39,94 --> 00:02:43,26
that is probably the A setting A in a dot

75
00:02:43,26 --> 00:02:46,45
var one equal to f f got an L A

76
00:02:46,45 --> 00:02:49,88
of negative 4 20 which is probably pointing at the

77
00:02:49,88 --> 00:02:53,01
B l a of negative to 10

78
00:02:53,17 --> 00:02:53,44
Well,

79
00:02:53,44 --> 00:02:53,74
actually,

80
00:02:53,74 --> 00:02:55,47
I don't know really which one could be the A

81
00:02:55,47 --> 00:02:57,16
Which one could be the be they could be either

82
00:02:57,16 --> 00:02:57,51
side

83
00:02:57,52 --> 00:02:59,1
Except for the fact that I guess I already know

84
00:02:59,1 --> 00:03:01,75
this is A because this is the one that has

85
00:03:01,75 --> 00:03:02,11
the F

86
00:03:02,11 --> 00:03:03,05
F in that cellar yet

87
00:03:03,14 --> 00:03:04,1
That's the B

88
00:03:04,18 --> 00:03:05,1
That's the A

89
00:03:05,11 --> 00:03:06,88
And it's calculating the address

90
00:03:06,89 --> 00:03:09,02
So stepping a little bit

91
00:03:09,02 --> 00:03:12,44
Let's see some concrete values subtract from the stack

92
00:03:12,45 --> 00:03:14,47
Now we're away down here

93
00:03:14,48 --> 00:03:19,26
Notice though that that stack subtraction was only 38 but

94
00:03:19,26 --> 00:03:21,76
we know we're accessing at 4 to 0

95
00:03:21,77 --> 00:03:24,29
So that means that again,

96
00:03:24,29 --> 00:03:26,66
the stack pointer kind of like we saw in the

97
00:03:26,66 --> 00:03:27,64
previous example,

98
00:03:27,65 --> 00:03:31,19
the stack pointer is actually going to be pointing partially

99
00:03:31,19 --> 00:03:34,63
into the variables and partially some of the space down

100
00:03:34,63 --> 00:03:36,19
at this lowest address is going to be in the

101
00:03:36,19 --> 00:03:36,79
red zone

102
00:03:36,83 --> 00:03:39,84
Let's go ahead and put that for one equal to

103
00:03:39,84 --> 00:03:40,12
f

104
00:03:40,12 --> 00:03:43,75
F and then calculate this address into RdX,

105
00:03:43,76 --> 00:03:46,47
calculate this address into our X

106
00:03:46,52 --> 00:03:49,38
Then it's going to move hex 40 into S E

107
00:03:49,38 --> 00:03:49,9
X

108
00:03:49,91 --> 00:03:52,43
So this is starting to look now like it's going

109
00:03:52,43 --> 00:03:53,72
to be the setup of the move

110
00:03:53,72 --> 00:03:54,36
S right,

111
00:03:54,36 --> 00:03:57,13
so S E X is the number of times something

112
00:03:57,13 --> 00:03:57,76
should occur

113
00:03:58,04 --> 00:04:00,77
RD is the destination index,

114
00:04:00,78 --> 00:04:02,84
R C is the source index

115
00:04:02,84 --> 00:04:07,29
So source is a destination is be the local variable

116
00:04:07,29 --> 00:04:07,6
B,

117
00:04:07,6 --> 00:04:08,82
which we just calculate them

118
00:04:08,83 --> 00:04:11,68
And so pretty much that's just going to go ahead

119
00:04:11,68 --> 00:04:12,65
and do the memcpy

120
00:04:15,84 --> 00:04:18,43
It's going to copy all of this stuff from source

121
00:04:18,43 --> 00:04:19,53
to destination

122
00:04:19,64 --> 00:04:21,76
I don't want to step through a hex 40 times

123
00:04:21,76 --> 00:04:25,07
so I'm going to go ahead and use until right

124
00:04:25,07 --> 00:04:29,35
here and just go past this

125
00:04:31,04 --> 00:04:31,63
Here we go,

126
00:04:31,64 --> 00:04:33,42
All sorts of copying going on

127
00:04:33,42 --> 00:04:35,83
But of course the majority of those structures were an

128
00:04:35,83 --> 00:04:36,76
initial list

129
00:04:37,14 --> 00:04:37,76
Consequently,

130
00:04:37,76 --> 00:04:40,55
it's just whatever garbage happens to be their copied garbage

131
00:04:40,55 --> 00:04:40,69
in,

132
00:04:40,69 --> 00:04:43,41
garbage out garbage from one to the other

133
00:04:43,42 --> 00:04:45,01
So what's happening now?

134
00:04:45,02 --> 00:04:45,28
Well,

135
00:04:45,28 --> 00:04:47,85
we've got RC into Rx,

136
00:04:48,34 --> 00:04:51,88
our DEA into RDX and Rx,

137
00:04:51,88 --> 00:04:53,07
which we said was R

138
00:04:53,07 --> 00:04:53,76
C

139
00:04:54,14 --> 00:04:55,16
From right here

140
00:04:55,94 --> 00:04:58,78
And keep in mind that because the move the red

141
00:04:58,78 --> 00:05:00,17
move as increments,

142
00:05:00,17 --> 00:05:00,44
R

143
00:05:00,44 --> 00:05:01,25
C and R

144
00:05:01,25 --> 00:05:01,76
D

145
00:05:02,14 --> 00:05:04,81
You expect that at the end of the repetition,

146
00:05:04,82 --> 00:05:08,79
they're basically going to be pointing Just past the end

147
00:05:08,79 --> 00:05:10,59
of each of their respective elements

148
00:05:10,66 --> 00:05:15,45
Now it's de referencing that and putting it into RCXR

149
00:05:15,45 --> 00:05:15,84
sorry,

150
00:05:16,12 --> 00:05:19,16
six step So what's going in there?

151
00:05:19,64 --> 00:05:20,06
All right

152
00:05:20,54 --> 00:05:23,98
E 20 So ultimately because this code is just going

153
00:05:23,98 --> 00:05:27,32
to be returning ace of base and because we are

154
00:05:27,53 --> 00:05:29,96
past the point of the memcpy,

155
00:05:30,44 --> 00:05:33,3
it's not clear what these assembly instructions are actually doing

156
00:05:33,31 --> 00:05:34,96
like what are they going to actually achieve?

157
00:05:35,44 --> 00:05:37,56
Step step step,

158
00:05:37,94 --> 00:05:43,2
they're calculating something but for no apparent reason because it

159
00:05:43,2 --> 00:05:45,85
S a base and Rx and then it returns out

160
00:05:45,86 --> 00:05:49,18
So that's probably one of those examples of the compiler

161
00:05:49,18 --> 00:05:51,55
Just going based on a template when it's doing an

162
00:05:51,55 --> 00:05:56,21
optimist assembly and getting some vestigial instructions in that don't

163
00:05:56,21 --> 00:05:57,25
actually have any point

164
00:05:57,84 --> 00:06:00,46
So let me fiddle around a little bit here to

165
00:06:00,46 --> 00:06:04,09
find the assembly that I was originally anticipating and then

166
00:06:04,09 --> 00:06:05,25
we'll come back and look at that

167
00:06:05,64 --> 00:06:05,95
Okay,

168
00:06:05,95 --> 00:06:08,47
so I remembered how exactly I got it,

169
00:06:08,48 --> 00:06:11,1
force it to go into meme copied and the answer

170
00:06:11,1 --> 00:06:14,82
was basically by blinding the compiler to the size of

171
00:06:14,82 --> 00:06:16,96
the memcpy that was going to occur when it

172
00:06:16,96 --> 00:06:19,79
was doing size of my struck then any sort of

173
00:06:19,79 --> 00:06:21,2
variants of the struck size

174
00:06:21,2 --> 00:06:23,79
The compilers would still always know exactly how much is

175
00:06:23,79 --> 00:06:24,43
being copied

176
00:06:24,44 --> 00:06:26,5
And so it could just simplify it down into a

177
00:06:26,5 --> 00:06:28,36
rough move us by blinding this,

178
00:06:28,36 --> 00:06:31,36
it basically forces the compiler to go into memcpy

179
00:06:31,36 --> 00:06:33,83
and let the memcpy heuristics about size,

180
00:06:33,84 --> 00:06:36,18
takeover and decide how much to copy

181
00:06:36,19 --> 00:06:38,62
So I've just changed this,

182
00:06:38,62 --> 00:06:41,6
struck size up to hex 20 because we know before

183
00:06:41,6 --> 00:06:43,83
that we had to bump up the size and stuff

184
00:06:43,84 --> 00:06:47,9
and we now have blinded the compiler to the size

185
00:06:47,9 --> 00:06:48,42
of copy

186
00:06:48,43 --> 00:06:51,46
So let's go ahead and compile this

187
00:06:52,94 --> 00:06:55,38
And I've got the optimizations on because I don't want

188
00:06:55,38 --> 00:06:59,32
to see the long form of the RV of zero

189
00:06:59,33 --> 00:07:00,82
RV of one access

190
00:07:00,83 --> 00:07:05,24
And then we would need to take and modify the

191
00:07:05,25 --> 00:07:07,72
RV of one that we're passing in for the amount

192
00:07:07,72 --> 00:07:09,05
that we want to copy

193
00:07:09,14 --> 00:07:09,55
Of course,

194
00:07:09,55 --> 00:07:09,73
you know,

195
00:07:09,73 --> 00:07:12,71
passing in a attacker controlled amount to copy into a

196
00:07:12,71 --> 00:07:14,72
memcpy is super bad idea,

197
00:07:14,73 --> 00:07:16,86
but we're just using this to learn right?

198
00:07:16,87 --> 00:07:18,56
So do as I say,

199
00:07:18,56 --> 00:07:20,26
not as I do and don't do that

200
00:07:20,64 --> 00:07:23,76
So let's go ahead and GDP this

201
00:07:26,74 --> 00:07:28,62
If we look at the overall function we see once

202
00:07:28,62 --> 00:07:31,9
again the optimization has turned it into a string too

203
00:07:31,9 --> 00:07:35,38
long instead of and then before the memcpy and

204
00:07:35,38 --> 00:07:37,2
this is actually a memcpy check

205
00:07:37,2 --> 00:07:40,34
So this is the fortified version which has some sanity

206
00:07:40,34 --> 00:07:42,37
checks to look for buffer overflows,

207
00:07:42,38 --> 00:07:43,67
basically it's going to,

208
00:07:43,67 --> 00:07:44,05
you know,

209
00:07:44,24 --> 00:07:48,43
passing the one copy the location of the destination for

210
00:07:48,43 --> 00:07:49,08
the copy,

211
00:07:49,08 --> 00:07:50,62
the source for the copy

212
00:07:50,76 --> 00:07:54,34
Sorry I was thinking removes the idea is not the

213
00:07:54,34 --> 00:07:54,93
destination,

214
00:07:54,93 --> 00:07:58,46
but it's the first argument which in this case happens

215
00:07:58,46 --> 00:07:59,3
to be the destination

216
00:07:59,3 --> 00:07:59,66
Anyway,

217
00:07:59,66 --> 00:08:01,49
so this is going to be the address of be

218
00:08:01,49 --> 00:08:02,38
where it's being copy,

219
00:08:02,38 --> 00:08:04,99
this is the address of a where it's copying from

220
00:08:05,0 --> 00:08:08,28
and the size is going to go into S E

221
00:08:08,28 --> 00:08:08,63
X

222
00:08:08,64 --> 00:08:13,15
So let's go ahead and the size actually came from

223
00:08:13,15 --> 00:08:16,15
strength long and so it's going into RDX and this

224
00:08:16,16 --> 00:08:19,81
is actually here because this memcpy check knows what

225
00:08:19,81 --> 00:08:22,47
the sizes of these buffers and throwing that in as

226
00:08:22,47 --> 00:08:23,39
a sanity check

227
00:08:23,4 --> 00:08:26,26
So let's go ahead and step over until we get

228
00:08:26,26 --> 00:08:27,36
to the memcpy

229
00:08:28,44 --> 00:08:30,6
Now let's step into the man copy

230
00:08:30,61 --> 00:08:31,96
So noah,

231
00:08:31,97 --> 00:08:35,25
a jump then now inside of here we have a

232
00:08:35,25 --> 00:08:37,98
compare of the RC X and R D X

233
00:08:37,99 --> 00:08:41,31
And this is basically again because we're in the the

234
00:08:41,31 --> 00:08:47,07
Czech version and if RC X is below R D

235
00:08:47,07 --> 00:08:49,86
X then it's going to fail

236
00:08:50,24 --> 00:08:51,37
So what do we have?

237
00:08:51,37 --> 00:08:57,66
We have RCX 24 and RdX is 20

238
00:08:58,14 --> 00:09:00,29
And so it's not going to go to the fail

239
00:09:00,29 --> 00:09:02,65
So that's essentially saying the 24 was hard coded in

240
00:09:02,65 --> 00:09:05,36
that's the size of the actual structure right now

241
00:09:05,74 --> 00:09:07,91
And it's basically saying if the amount that you're trying

242
00:09:07,91 --> 00:09:11,48
to copy is greater than or equal to this value

243
00:09:11,49 --> 00:09:11,92
sorry,

244
00:09:11,93 --> 00:09:14,29
greater than greater than or equal to

245
00:09:14,66 --> 00:09:18,72
So if this value next 24 is above or equal

246
00:09:18,72 --> 00:09:20,1
to 20,

247
00:09:20,1 --> 00:09:22,21
then basically that RCX,

248
00:09:22,21 --> 00:09:23,57
which was the hard coded size,

249
00:09:23,58 --> 00:09:26,32
if that hard coded size is below the amount that

250
00:09:26,32 --> 00:09:27,12
you want to copy,

251
00:09:27,12 --> 00:09:29,54
then that's an error condition because you're over copying,

252
00:09:29,54 --> 00:09:30,79
you're going to smash the stack

253
00:09:30,8 --> 00:09:32,72
So that should not be true here

254
00:09:32,72 --> 00:09:34,36
So we will not take the jump below,

255
00:09:34,84 --> 00:09:36,23
We will fall through

256
00:09:36,24 --> 00:09:37,49
And what are we going to see?

257
00:09:37,49 --> 00:09:41,68
We're going to see a move of Rx already to

258
00:09:41,68 --> 00:09:42,6
our X

259
00:09:42,61 --> 00:09:45,58
And then a compare against text 20

260
00:09:45,59 --> 00:09:46,26
Alright,

261
00:09:46,27 --> 00:09:49,66
so compare against sex 20 and then jump below

262
00:09:50,14 --> 00:09:53,65
Well RDX is exactly hex 20

263
00:09:54,04 --> 00:09:55,84
So it's not going to take the jump below because

264
00:09:55,84 --> 00:09:56,86
it's actually equal

265
00:09:56,87 --> 00:09:58,42
Can also see a hex 40

266
00:09:58,42 --> 00:10:00,54
So that might give us a hint for the next

267
00:10:00,54 --> 00:10:02,05
possible size we might use

268
00:10:02,06 --> 00:10:05,45
So it's not taking that and it's falling through to

269
00:10:05,45 --> 00:10:06,36
the X 40

270
00:10:06,74 --> 00:10:09,38
The next thing it's going to do is jump above

271
00:10:09,39 --> 00:10:13,67
So if SDX is above 40 and it'll jump to

272
00:10:13,67 --> 00:10:14,56
this location,

273
00:10:14,94 --> 00:10:16,83
But SDX is not above 40

274
00:10:16,83 --> 00:10:19,67
So it's going to instead fall through and there we

275
00:10:19,67 --> 00:10:24,48
have our even scarier form of these whatever assembly instructions

276
00:10:24,48 --> 00:10:26,66
that we don't know anything about which you're using,

277
00:10:26,66 --> 00:10:27,21
registers,

278
00:10:27,21 --> 00:10:28,26
we don't know anything about

279
00:10:28,64 --> 00:10:29,96
So we don't like that

280
00:10:29,97 --> 00:10:33,12
Let's get out of here and let's crank the size

281
00:10:33,12 --> 00:10:36,46
above That hex 40 before that we saw just a

282
00:10:36,46 --> 00:10:37,16
second ago

283
00:10:39,84 --> 00:10:43,7
So Up to 40 total size is going to be

284
00:10:43,7 --> 00:10:44,61
44

285
00:10:44,62 --> 00:10:48,97
And let's go ahead and instead of editing our GDP

286
00:10:48,97 --> 00:10:51,95
thing let's just go ahead and use start within GDP

287
00:10:52,84 --> 00:10:58,14
so compile it GDP it it's too annoying to change

288
00:10:58,14 --> 00:11:01,36
that every time and let's just do start And Hex

289
00:11:01,36 --> 00:11:03,66
40 would be 64

290
00:11:06,94 --> 00:11:10,02
All right so continue through here until we get to

291
00:11:10,02 --> 00:11:13,66
the memcpy step into it

292
00:11:15,44 --> 00:11:18,06
Going to have the sanity check which we should bypass

293
00:11:18,06 --> 00:11:20,41
because we're not trying to copy too much

294
00:11:20,42 --> 00:11:22,85
We're copying 40 when the total size is 44

295
00:11:24,94 --> 00:11:27,35
We're going to bypass the hex 20 check we already

296
00:11:27,35 --> 00:11:29,81
saw before because we're not below hex 20

297
00:11:29,98 --> 00:11:32,47
We're going to bypass the hex 40

298
00:11:32,47 --> 00:11:36,53
Check not bypass for exactly we're going to Actually be

299
00:11:36,53 --> 00:11:37,08
above X

300
00:11:37,08 --> 00:11:39,32
40 so we're going to take that jump instead of

301
00:11:39,32 --> 00:11:41,86
falling through to these instructions we don't know anything about

302
00:11:42,84 --> 00:11:46,12
So if we step we expect we're going to that

303
00:11:46,12 --> 00:11:49,02
was a above check not a above or equal

304
00:11:49,02 --> 00:11:49,63
So X

305
00:11:49,63 --> 00:11:50,5
40 doesn't do it,

306
00:11:50,5 --> 00:11:52,6
we got to be above X

307
00:11:52,6 --> 00:11:56,68
40 so started again And instead of 64 let's go

308
00:11:56,68 --> 00:12:05,91
to 68 step into memcpy Step into the 40

309
00:12:05,91 --> 00:12:10,17
check and we are now above 40 because we're 44

310
00:12:10,3 --> 00:12:12,97
step And take the jump

311
00:12:12,98 --> 00:12:17,08
Now we see some comparisons against text 1000 X

312
00:12:17,08 --> 00:12:19,04
180

313
00:12:19,07 --> 00:12:23,56
So my general sense is that in general a red

314
00:12:23,56 --> 00:12:26,33
move s is going to be something that they might

315
00:12:26,34 --> 00:12:28,4
do when you get to really large sizes

316
00:12:28,4 --> 00:12:29,7
If you have smaller sizes,

317
00:12:29,7 --> 00:12:33,57
it might be more efficient to use whatever these vectorized

318
00:12:33,57 --> 00:12:34,97
move instructions are

319
00:12:35,24 --> 00:12:37,91
And so let's just try to crank up the size

320
00:12:37,91 --> 00:12:43,41
to actually be well we want above Hex 1000

321
00:12:43,41 --> 00:12:46,91
So let's go ahead and quit out and change out

322
00:12:46,91 --> 00:12:47,56
our size

323
00:12:50,34 --> 00:12:53,06
So let's make it hex 1000 here

324
00:12:54,54 --> 00:12:57,15
And that means the total size will be 1000

325
00:12:57,54 --> 00:12:58,16
Poor,

326
00:13:00,44 --> 00:13:01,55
compile it again,

327
00:13:02,64 --> 00:13:06,83
G B s and start In decimal

328
00:13:06,83 --> 00:13:08,91
Hex 1000 is 40 96,

329
00:13:08,92 --> 00:13:11,56
so plus four is 40 100

330
00:13:14,54 --> 00:13:17,3
And let's go ahead and step until we make our

331
00:13:17,3 --> 00:13:19,31
way to the memcpy and I went too far

332
00:13:19,32 --> 00:13:20,45
So try it again

333
00:13:23,24 --> 00:13:26,05
The reason I'm not just using until or setting a

334
00:13:26,05 --> 00:13:28,44
breakpoint is because actually as we're changing the sizes,

335
00:13:28,44 --> 00:13:31,09
the assembly is going to be changing subtly

336
00:13:31,09 --> 00:13:33,86
So I can't use the exact same addresses each time

337
00:13:33,87 --> 00:13:35,32
All right now,

338
00:13:35,32 --> 00:13:37,25
let's step into this

339
00:13:38,44 --> 00:13:42,49
We are Requesting a copy of 1004 in the maximum

340
00:13:42,49 --> 00:13:43,41
size is 1000

341
00:13:43,41 --> 00:13:48,45
and force we bypassed sanity check and we go past

342
00:13:48,45 --> 00:13:50,17
the 20 go past the 40

343
00:13:50,18 --> 00:13:51,75
Get to the hex 1000

344
00:13:51,75 --> 00:13:56,63
So RDX is 1004 above 1000

345
00:13:56,63 --> 00:13:57,8
The answer is yes

346
00:13:57,81 --> 00:14:00,06
So we step and we're going to take the jump

347
00:14:00,06 --> 00:14:00,45
above

348
00:14:01,24 --> 00:14:01,53
All right

349
00:14:01,53 --> 00:14:02,35
What do we see now?

350
00:14:02,74 --> 00:14:06,4
We see a compare of some location relative to rip

351
00:14:06,41 --> 00:14:07,4
and against

352
00:14:07,41 --> 00:14:07,99
RdX

353
00:14:07,99 --> 00:14:10,0
Rdx is our next 1000 and four

354
00:14:10,01 --> 00:14:12,58
And so compare And then a jump above

355
00:14:12,59 --> 00:14:12,74
Well,

356
00:14:12,74 --> 00:14:14,16
let's see what value is here

357
00:14:16,24 --> 00:14:18,65
Giant six

358
00:14:19,84 --> 00:14:20,95
That location

359
00:14:21,44 --> 00:14:21,7
All right

360
00:14:21,71 --> 00:14:22,59
It's zero

361
00:14:22,6 --> 00:14:25,14
So is our ex 1000 and four going to be

362
00:14:25,14 --> 00:14:25,92
above zero

363
00:14:25,93 --> 00:14:26,32
Yes,

364
00:14:26,32 --> 00:14:26,84
it is

365
00:14:26,85 --> 00:14:27,4
And so,

366
00:14:27,4 --> 00:14:29,57
this jump will always be taken at this point

367
00:14:29,57 --> 00:14:32,63
I started getting confused because I don't know why

368
00:14:32,63 --> 00:14:34,24
I don't know if something's different here

369
00:14:34,25 --> 00:14:36,36
Maybe the first time I actually look through this code

370
00:14:36,36 --> 00:14:38,75
I just literally looked at the code and I stopped

371
00:14:38,75 --> 00:14:39,42
when I saw,

372
00:14:39,43 --> 00:14:40,03
oh hey,

373
00:14:40,04 --> 00:14:42,69
we made our way to the center of memcpy

374
00:14:42,69 --> 00:14:45,8
We found the rep move as But this fact that

375
00:14:45,8 --> 00:14:48,99
this is zero here means that it's not actually reachable

376
00:14:48,99 --> 00:14:51,86
So we want to essentially fall through here and instead

377
00:14:51,86 --> 00:14:54,39
we're always going to be taking this jump above because

378
00:14:54,4 --> 00:14:57,26
everything's above or equal to zero

379
00:14:58,04 --> 00:15:00,2
So what we really want here is we want to

380
00:15:00,2 --> 00:15:03,05
somehow fall through to the rep move us and that's

381
00:15:03,05 --> 00:15:05,57
not really possible unless we alter this memory

382
00:15:05,58 --> 00:15:07,85
So let's go ahead and alter that memory

383
00:15:09,04 --> 00:15:17,96
So sad this all along at this particular address to

384
00:15:18,16 --> 00:15:23,2
some value so that 1004 is not above or equal

385
00:15:23,2 --> 00:15:24,24
So I don't know,

386
00:15:24,25 --> 00:15:25,25
68,000

387
00:15:28,64 --> 00:15:30,19
And if we read it again,

388
00:15:30,19 --> 00:15:32,76
we can see that it got set to hex 8000

389
00:15:32,77 --> 00:15:36,18
And so now this compare will not lead to an

390
00:15:36,18 --> 00:15:37,01
above or equal

391
00:15:37,01 --> 00:15:39,59
So if we step over that and we step,

392
00:15:39,6 --> 00:15:41,66
we now expect that this thing will not jump

393
00:15:42,04 --> 00:15:46,34
And now we have this compare of RD two RC

394
00:15:46,34 --> 00:15:49,3
And we want to see if RD is below take

395
00:15:49,3 --> 00:15:50,33
the jump again

396
00:15:50,33 --> 00:15:52,85
We want to just be kind of falling through here

397
00:15:53,02 --> 00:15:56,55
And so we want to jump not to be taken

398
00:15:56,55 --> 00:15:58,61
We want this jump equal not to be taken

399
00:15:58,62 --> 00:16:00,53
We want this jump below not to be taken

400
00:16:00,53 --> 00:16:02,01
And we want to kind of fall through to the

401
00:16:02,02 --> 00:16:02,71
rep movies

402
00:16:02,77 --> 00:16:04,84
So R D R C

403
00:16:04,84 --> 00:16:05,21
Well,

404
00:16:05,21 --> 00:16:09,13
rdr crr locations for A and B R D

405
00:16:09,13 --> 00:16:10,92
Is that below RC?

406
00:16:10,93 --> 00:16:14,18
So it's BF 10 vs

407
00:16:14,19 --> 00:16:18,38
Cf 20 so that looks below to me,

408
00:16:18,38 --> 00:16:23,91
so that's going to be taken step step and because

409
00:16:23,91 --> 00:16:26,21
I wasn't paying attention to where it actually jumped,

410
00:16:26,26 --> 00:16:28,1
we successfully jumped to a wrong move

411
00:16:28,1 --> 00:16:30,5
S actually when I was screwing around with this,

412
00:16:30,5 --> 00:16:31,75
I was playing with orders,

413
00:16:31,75 --> 00:16:34,39
I changed the order of copying beat A instead of

414
00:16:34,39 --> 00:16:34,94
A to B

415
00:16:34,94 --> 00:16:36,14
And then it would fall through

416
00:16:36,14 --> 00:16:39,31
But this turns out to be a much simpler version

417
00:16:39,31 --> 00:16:39,73
of that

418
00:16:39,74 --> 00:16:40,1
So,

419
00:16:40,11 --> 00:16:40,66
great!

420
00:16:40,71 --> 00:16:43,73
Now we are going to proceed into the rep move

421
00:16:43,73 --> 00:16:44,79
s of course,

422
00:16:44,79 --> 00:16:48,57
because I don't want to go through that Brazilian times

423
00:16:48,94 --> 00:16:51,07
I'm just going to go over it

424
00:16:51,84 --> 00:16:57,81
So until there and we successfully got past it

425
00:16:57,82 --> 00:17:01,65
And now we are back inside of main having successfully

426
00:17:01,65 --> 00:17:02,66
done the memcpy,

427
00:17:03,54 --> 00:17:06,04
that was the journey to the center of memcpy

428
00:17:06,05 --> 00:17:06,96
in Lenox

429
00:17:07,64 --> 00:17:12,36
And basically you can see how similar to the Fantasy

430
00:17:12,36 --> 00:17:15,7
library on Windows and Visual Studio

431
00:17:15,71 --> 00:17:18,6
There's a variety of heuristics that the compiler uses in

432
00:17:18,6 --> 00:17:21,08
order to decide what the fastest possible way to do

433
00:17:21,08 --> 00:17:21,92
mm copy is

434
00:17:21,93 --> 00:17:25,19
And they just make those optimizations into library so that

435
00:17:25,19 --> 00:17:27,9
only when the compiler knows exactly how much size it's

436
00:17:27,9 --> 00:17:28,32
copying,

437
00:17:28,32 --> 00:17:30,44
does it just inlined at the rest of the

438
00:17:30,44 --> 00:17:30,82
time,

439
00:17:30,83 --> 00:17:32,74
you just jump out to the library and the library

440
00:17:32,74 --> 00:17:34,27
deals for with it for you

