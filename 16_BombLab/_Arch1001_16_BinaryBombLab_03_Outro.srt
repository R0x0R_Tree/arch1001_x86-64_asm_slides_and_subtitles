1
00:00:00,880 --> 00:00:06,319
okay so before you go on

2
00:00:03,040 --> 00:00:07,279
with the binary bond if you think of

3
00:00:06,319 --> 00:00:09,840
yourself as an

4
00:00:07,279 --> 00:00:10,800
expert or if you maybe took this course

5
00:00:09,840 --> 00:00:12,639
a long time ago

6
00:00:10,800 --> 00:00:15,440
in open security training one and you

7
00:00:12,639 --> 00:00:18,080
haven't already know stuff about x86

8
00:00:15,440 --> 00:00:19,680
you have the opportunity to do the bom

9
00:00:18,080 --> 00:00:22,640
lab in expert mode

10
00:00:19,680 --> 00:00:24,800
in expert mode you remove the helpful

11
00:00:22,640 --> 00:00:26,560
symbols the debugging information

12
00:00:24,800 --> 00:00:28,560
which you would not actually have if you

13
00:00:26,560 --> 00:00:29,599
were targeting some particular piece of

14
00:00:28,560 --> 00:00:31,920
real software

15
00:00:29,599 --> 00:00:33,440
or some particular piece of real malware

16
00:00:31,920 --> 00:00:34,480
they're typically unless they really

17
00:00:33,440 --> 00:00:36,160
really screw up

18
00:00:34,480 --> 00:00:37,920
they're not going to generally provide

19
00:00:36,160 --> 00:00:39,840
that information now for things like

20
00:00:37,920 --> 00:00:42,640
windows operating system microsoft does

21
00:00:39,840 --> 00:00:44,320
provide some of the simple information

22
00:00:42,640 --> 00:00:45,760
but that's neither here nor there so if

23
00:00:44,320 --> 00:00:48,320
you want to run this as an

24
00:00:45,760 --> 00:00:49,520
expert then go ahead and strip these

25
00:00:48,320 --> 00:00:51,440
symbols from the bomb

26
00:00:49,520 --> 00:00:53,120
running the strip command on linux and

27
00:00:51,440 --> 00:00:55,680
unix type systems

28
00:00:53,120 --> 00:00:57,360
or just straight up delete the dot pdb

29
00:00:55,680 --> 00:00:59,760
file the portable debug file

30
00:00:57,360 --> 00:01:01,120
off of windows and try it without the

31
00:00:59,760 --> 00:01:02,000
debugging information i think you'll

32
00:01:01,120 --> 00:01:05,280
find that it's

33
00:01:02,000 --> 00:01:08,000
exceedingly much more difficult

34
00:01:05,280 --> 00:01:09,840
alright now just a couple words of hints

35
00:01:08,000 --> 00:01:11,680
before you continue on

36
00:01:09,840 --> 00:01:14,159
of course documentation is your friend

37
00:01:11,680 --> 00:01:16,159
so if you forgot or just never knew

38
00:01:14,159 --> 00:01:17,200
some of the standard c library things

39
00:01:16,159 --> 00:01:19,520
you encounter like

40
00:01:17,200 --> 00:01:21,280
s scanf then you should of course you

41
00:01:19,520 --> 00:01:22,799
know google it read the man page read

42
00:01:21,280 --> 00:01:25,280
the microsoft documentation

43
00:01:22,799 --> 00:01:26,960
whatever but you should you should look

44
00:01:25,280 --> 00:01:28,880
up for opportunities to grab

45
00:01:26,960 --> 00:01:30,640
public and available information

46
00:01:28,880 --> 00:01:32,479
wherever possible

47
00:01:30,640 --> 00:01:33,920
and also this is a very big one

48
00:01:32,479 --> 00:01:34,640
especially for those of you who are

49
00:01:33,920 --> 00:01:37,280
taking this

50
00:01:34,640 --> 00:01:39,360
to eventually go towards the reverse

51
00:01:37,280 --> 00:01:41,119
engineering type learning tracks

52
00:01:39,360 --> 00:01:44,479
it's important to always you know keep

53
00:01:41,119 --> 00:01:47,439
in mind what exactly is your goal here

54
00:01:44,479 --> 00:01:48,880
it's extremely easy to get lost in the

55
00:01:47,439 --> 00:01:51,040
weeds when you're looking at assembly

56
00:01:48,880 --> 00:01:52,799
language and especially as someone who

57
00:01:51,040 --> 00:01:54,159
might be new to assembly language

58
00:01:52,799 --> 00:01:55,520
you're going to be like wanting to look

59
00:01:54,159 --> 00:01:56,799
at what's this instruction do what's

60
00:01:55,520 --> 00:01:57,840
that instruction do what's that as

61
00:01:56,799 --> 00:01:59,520
instruction do

62
00:01:57,840 --> 00:02:01,840
and that's exactly how you get stuck

63
00:01:59,520 --> 00:02:03,200
lost in the weeds and you lose sight of

64
00:02:01,840 --> 00:02:04,240
the goal

65
00:02:03,200 --> 00:02:05,680
frequently is the case when you're

66
00:02:04,240 --> 00:02:06,479
reverse engineering you want to do like

67
00:02:05,680 --> 00:02:08,479
a very

68
00:02:06,479 --> 00:02:10,479
broad like scan of everything to just

69
00:02:08,479 --> 00:02:12,160
see like just generally what's there

70
00:02:10,479 --> 00:02:13,680
and then narrowly focus on something but

71
00:02:12,160 --> 00:02:14,879
then you get too deep into it and so you

72
00:02:13,680 --> 00:02:16,080
got to come back up and you've got to

73
00:02:14,879 --> 00:02:16,800
look at the big picture and then you go

74
00:02:16,080 --> 00:02:19,360
down deep

75
00:02:16,800 --> 00:02:20,959
and you come back up so that's a common

76
00:02:19,360 --> 00:02:22,480
thing and the hint here is

77
00:02:20,959 --> 00:02:24,080
you know focus on what your goal is for

78
00:02:22,480 --> 00:02:25,920
this particular exercise

79
00:02:24,080 --> 00:02:27,520
the goal is not read every single

80
00:02:25,920 --> 00:02:29,680
assembly line and understand

81
00:02:27,520 --> 00:02:30,720
every single assembly instruction the

82
00:02:29,680 --> 00:02:33,599
goal is

83
00:02:30,720 --> 00:02:34,000
get the program to run without exploding

84
00:02:33,599 --> 00:02:36,400
so

85
00:02:34,000 --> 00:02:38,080
you need to focus on what are the exit

86
00:02:36,400 --> 00:02:40,800
conditions what are the ways that you

87
00:02:38,080 --> 00:02:42,640
avoid those calls to explode bomb

88
00:02:40,800 --> 00:02:44,400
and then work backwards from that that's

89
00:02:42,640 --> 00:02:46,080
why it's reverse engineering so walk

90
00:02:44,400 --> 00:02:46,720
backwards from the exit conditions and

91
00:02:46,080 --> 00:02:48,560
figure out

92
00:02:46,720 --> 00:02:50,080
how do you avoid those paths how do you

93
00:02:48,560 --> 00:02:51,920
provide inputs

94
00:02:50,080 --> 00:02:54,319
that force the control flow down the

95
00:02:51,920 --> 00:02:56,239
particular path that you want

96
00:02:54,319 --> 00:02:58,560
all right and with that said i wish you

97
00:02:56,239 --> 00:03:02,560
good luck on your adventures into

98
00:02:58,560 --> 00:03:02,560
the heart of the binary bomb

